//HTML elements
const getLoanElement = document.getElementById("getLoan");
const outstandingLoanElement = document.getElementById("outstandingLoan");
const bankBalanceElement = document.getElementById("bankBalance");
const bankButtonElement = document.getElementById("bankButton");
const workButtonElement = document.getElementById("workButton");
const payAmountElement = document.getElementById("payAmount");
const payLoanElement = document.getElementById("payLoan");
const laptopListElement = document.getElementById("laptopList");
const laptopInfoElement = document.getElementById("laptopInfo");
const laptopImageElement = document.getElementById("laptopImage");
const laptopNameElement = document.getElementById("laptopName");
const laptopDescElement = document.getElementById("laptopDesc");
const laptopPriceElement = document.getElementById("laptopPrice");
const buyLaptopElement = document.getElementById("buyLaptop");

//Various balance
let bankBalance = Number(prompt("Please enter bank balance"));
let outstandingLoan = 0;
let payAmount = 0;
const computers = [];

//Starting balances
changeInnerHTML("Bank Balance: ", bankBalance, bankBalanceElement);
changeInnerHTML("Pay: ", 0, payAmountElement);

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then((response) => {
        return response.json()
    })
    .then((laptopResponse) => {
        computers.push(...laptopResponse);
        renderLaptopList(computers);
    })
    .then(() => {
        laptopImageElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + computers[0].image;
        laptopNameElement.innerHTML = computers[0].title;
        laptopDescElement.innerHTML = computers[0].description;
        laptopPriceElement.innerHTML = computers[0].price + "kr";
        for (const feature of computers[0].specs) {
            laptopInfoElement.innerHTML += feature + "<br>";
        };
    });

    /**
     * 
     * @param { string } newMessage 
     * @param { number } newValue 
     * @param { HTMLElement } changeElement 
     */
    //Changes the text in the html element
function changeInnerHTML(newMessage, newValue, changeElement){
    changeElement.innerHTML = newMessage + newValue + "kr";
};

//Get object from computers array with the id found
const findLaptopById = (laptops, laptopId) => {
    return laptops.find(function(laptop){
        return laptop.id === Number(laptopId);
    });
};
//Get object from computers array with the title found
const findLaptopByTitle = (laptops, laptopName) => {
    return laptops.find(function(laptop){
        return laptop.title === String(laptopName);
    });
}

getLoanElement.addEventListener("click", function(){
    if (outstandingLoan > 0) { //Checks if the user already has a loan
        alert("You already have a loan");
        return;
    };

    let loanAmount = parseInt(prompt("Enter loan Amount"));
    
    if(loanAmount > (bankBalance*2)){ //Checking if the loan amount is more than double of bank balance
        alert("Loan amount too high");
        return;
    };

    outstandingLoan += loanAmount;

    changeInnerHTML("Outstanding Loan Amount: ", loanAmount, outstandingLoanElement);
    changeInnerHTML("Balance: ", bankBalance, bankBalanceElement);

    outstandingLoanElement.style.visibility = 'visible';
});

workButtonElement.addEventListener("click", function(){
    if(outstandingLoan > 0){ //makes the pay loan button visible if the user has a loan
        payLoanElement.style.visibility = "visible";
    };

    payAmount += 100;
    changeInnerHTML("Pay: ", payAmount, payAmountElement);
});

bankButtonElement.addEventListener("click", function(){
    if(outstandingLoan > 0) { //Checks if there is a loan 
        outstandingLoan -= payAmount*0.1;
        payAmount -= payAmount*0.1;
    };

    payLoanElement.style.visibility = "hidden";

    bankBalance += payAmount;
    changeInnerHTML("Balance: ", bankBalance, bankBalanceElement);

    payAmount = 0;
    changeInnerHTML("Pay: ", payAmount, payAmountElement);
    changeInnerHTML("Outstanding Loan: ", outstandingLoan, outstandingLoanElement);
});

payLoanElement.addEventListener("click", function(){
    if(payAmount > outstandingLoan){ //Checks if the pay amount is bigger than the loan
        payAmount -= outstandingLoan;
        outstandingLoan = 0;
    } 
    else if(payAmount === outstandingLoan){ //Checks if the pay amount and the loan is exactly the same
        outstandingLoan = 0;
        payAmount = 0;
    } 
    else { //Checks if the loan is bigger than the pay amount
        outstandingLoan -= payAmount;
        payAmount = 0;
    };

    changeInnerHTML("Outstanding Loan Amount: ", outstandingLoan, outstandingLoanElement);

    if(payAmount > 0){
        payLoanElement.style.visibility = "hidden";
    };
   
    changeInnerHTML("Pay: ", payAmount, payAmountElement);
});

/**
 * 
 * @param { [] } laptops 
 */
//Function that renders a laptop list, and adds a option in the select box
function renderLaptopList(laptops){
    for (const laptop of laptops){
        const html = `<option id = ${laptop.id}> ${laptop.title}</option>`;
        laptopListElement.insertAdjacentHTML("beforeend", html);
    };
};


laptopListElement.addEventListener("change", function(e){
    laptopInfoElement.innerHTML = "";

    const laptopId = e.target.options[e.target.selectedIndex].getAttribute('id');
    const selectedLaptop = findLaptopById(computers, laptopId);

    for (const feature of selectedLaptop.specs) {
        laptopInfoElement.innerHTML += feature + "<br>";
    };

    const imageSrc = "https://noroff-komputer-store-api.herokuapp.com/" + selectedLaptop.image;

    laptopImageElement.src = imageSrc;
    laptopNameElement.innerHTML = selectedLaptop.title;
    laptopDescElement.innerHTML = selectedLaptop.description;
    laptopPriceElement.innerHTML = selectedLaptop.price + "kr";
});

buyLaptopElement.addEventListener("click", function() {
    const chosenLaptop = findLaptopByTitle(computers, laptopNameElement.innerHTML);
    
    if (bankBalance >= chosenLaptop.price) { //Checks if the user can buy the selected computer
        if(chosenLaptop.stock > 0){ //Checks if laptop is in stock
            bankBalance -= chosenLaptop.price;

            alert("You are the new owner of this computer!");
            
            chosenLaptop.stock -= 1;
        } 
        else {
            alert("Out of stock");
        };
    } 
    else {
        alert("You do not have enough money available to purchase this computer");
    };
   
    changeInnerHTML("Bank Balance: ", bankBalance, bankBalanceElement);
}); 