CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Technologies
* Installation 

INTRODUCTION
------------
This is my solution for assignment 1 - "Komputer Store App". The project is divided in 4 parts. First part is the upper left div-box called bank. The bank balance and get loan-button are located in this area. The outstanding loan parapgraph is also located in this area, but is only visible when the user has taken out a loan. When the page is first loaded, the user gets a propmt asking for their bank balance. This information is then displayed in the bank balance-paragraph. 
The div-box in the middle is called the work-box. The work-, pay-button and pay-"balance" are located in this section. The pay loan-button is also located in this div, but is only available when there is an active loan. 
The upper right div-box is the laptop select section. Here is a select-box where the user can choose which laptop they want displayed. The features of the selected laptop is also displayed in this specific div-box. The laptop list is rendered through a fetch function which turns the response into json objects. When the page is loaded, the first element in the list is automatically displayed. 
The Last section is the bottom section, which i called the purchase-box. This section is divided in three sections as well. The left side displays the image of the selected computer. The middle part displays the title and the description of the selected computer. And the right side displays the price of the selected computer. The right side also has a buy laptop-button. The user can press this button and buy the computer as long as it is in stock. If they can not afford the computer, they will be notified of it. 

REQUIREMENTS
------------
Was required to use vanilla javascript

TECHNOLOGIES
------------
Javascript
HTML 
CSS
JSON 

INSTALLATION
------------
git
npm install
json